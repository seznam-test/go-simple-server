# steps
1. install ubuntu VM
Generická minimální instalace ubuntu serveru
2. install k3s without terafic
Nainstalováno pomocí k3s quickstart scriptu s paramterem pro skip instalace traefik ingress controlleru
3. install kubernetes nginx ingress controller
Nainstalováno pomocí berbone kubectl configu + přidání service 
4. configure local hosts file
Díky tomu že nemám pro tyto učely připravené dns tak jsem si lokálně na pc nastavil hosts záznamy
5. install argocd - https://argocd.test.local/login?return_url=https%3A%2F%2Fargocd.test.local%2Fapplications 
Defaultní instalace argocd 
6. Add this gitlab repository to argocd repositories with GUI
7. create test app that deploy kubectl files with app folder in repositry 
8. enable bind for controller/proxy/scheduler to allow monitoring this services
9. download CRDS and deploy to monitoring namespace
10. deploy prometheus using argocd
11. create service monitor resource for monitoring app
12. crearte grafana dashboard and use jmetter to generate data

# services
Argocd running in argocd namespace with ingress served by nginx url http://argocd.test.local/ password in secret argocd-initial-admin-secret username admin

Grafana is running in monitoring namespace with ingress served by nginx url http://grafana.test.local/ password is prom-operator and username admin


# sw in cluster
Nginx ingress - from yaml - added service for port 80+443
Argocd - from yaml - added ingress object
Prometheus kube stack mlonitoring - using argocd
Semple app - using argocd 
    - deploy app as deployment with replicas
    - create service and ingress http://app.test.local/
    - create servicemonitor for monitoring
    - deploy dashboard to grafana using sidecar from grafana that load configmaps as dashboards

# testing
Testing using apache jmeter 