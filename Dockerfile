ARG IMAGE=scratch


# build docker image
FROM golang:1.22.4-alpine3.20 as builder

WORKDIR /build

COPY . .

# Installs Go dependencies
RUN go mod download
# build go app
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /run


# create minimal docker image for run
FROM ${IMAGE}

WORKDIR /

COPY --chmod=0755 --from=builder /run /run
EXPOSE 8080

CMD [ “/run/go-simple-server” ]
